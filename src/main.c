#define G_LOG_DOMAIN "main"

#include "config.h"

#include <glib/gi18n.h>
#include <libgplant-core.h>
#include <libgplant-gui.h>

gint
main (gint argc,
      gchar *argv[])
{
  g_autoptr(GPlantApplication) app = NULL;
  gint ret;

  app = g_object_new (GPLANT_TYPE_APPLICATION,
                      "application-id", "org.plantd.GPlant",
                      "resource-base-path", "/org/plantd/event",
                      NULL);

  ret = g_application_run (G_APPLICATION (app), argc, argv);

  return ret;
}
