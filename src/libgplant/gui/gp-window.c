#include <glib/gi18n.h>

#include "gp-window.h"

struct _GPlantWindow
{
  DzlApplicationWindow  parent_instance;
  GtkHeaderBar         *header_bar;
  GtkStack             *stack;
};

G_DEFINE_TYPE (GPlantWindow, gplant_window, DZL_TYPE_APPLICATION_WINDOW)

static const DzlShortcutEntry shortcuts[] = {
  {
    "org.gplant.window.Fullscreen", 0, "F11",
    N_("Editing"),
    N_("General"),
    N_("Fullscreen"),
    N_("Toggle window fullscreen")
  },
};

static void
gplant_window_class_init (GPlantWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/plantd/libgplant-gui/ui/gp-window.ui");
  gtk_widget_class_bind_template_child (widget_class, GPlantWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, GPlantWindow, stack);
}

static void
gplant_window_init (GPlantWindow *self)
{
  DzlShortcutController *controller;

  gtk_widget_init_template (GTK_WIDGET (self));

  dzl_shortcut_manager_add_shortcut_entries (NULL, shortcuts, G_N_ELEMENTS (shortcuts), NULL);
  controller = dzl_shortcut_controller_find (GTK_WIDGET (self));
  dzl_shortcut_controller_add_command_action (controller,
                                              "org.gplant.window.Fullscreen",
                                              NULL,
                                              0,
                                              "win.fullscreen");

  g_signal_connect_swapped (self,
                            "key-press-event",
                            G_CALLBACK (dzl_shortcut_manager_handle_event),
                            dzl_shortcut_manager_get_default ());
}

GtkWidget *
gplant_window_new (void)
{
  return g_object_new (GPLANT_TYPE_WINDOW, NULL);
}
