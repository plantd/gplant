#pragma once

#include <gtk/gtk.h>
#include <libgplant-core.h>

#define GPLANT_GUI_INSIDE

#include "gp-application.h"
//#include "gp-application-addin.h"
//#include "gp-header-bar.h"
//#include "gp-preferences-addin.h"
//#include "gp-preferences-surface.h"
//#include "gp-preferences-window.h"
#include "gp-window.h"
//#include "gp-window-addin.h"
//#include "gp-workspace.h"
//#include "gp-workspace-addin.h"

#undef GPLANT_GUI_INSIDE
