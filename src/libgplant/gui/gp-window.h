#pragma once

#include <dazzle.h>

G_BEGIN_DECLS

#define GPLANT_TYPE_WINDOW (gplant_window_get_type())

G_DECLARE_FINAL_TYPE (GPlantWindow, gplant_window, GPLANT, WINDOW, DzlApplicationWindow)

GtkWidget *gplant_window_new (void);

G_END_DECLS
