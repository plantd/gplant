#pragma once

#include <dazzle.h>
#include <libgplant-core.h>

G_BEGIN_DECLS

#define GPLANT_TYPE_APPLICATION (gplant_application_get_type())

G_DECLARE_FINAL_TYPE (GPlantApplication, gplant_application, GPLANT, APPLICATION, DzlApplication)

G_END_DECLS
