#pragma once

#if !defined (GPLANT_PLUGINS_INSIDE) && !defined (GPLANT_PLUGINS_COMPILATION)
# error "Only <libgplant-plugins.h> can be included directly."
#endif

#include <libpeas/peas.h>
#include <libgplant-core.h>

G_BEGIN_DECLS

#define GPLANT_TYPE_EXTENSION_ADAPTER (gplant_extension_adapter_get_type())

G_DECLARE_FINAL_TYPE (GPlantExtensionAdapter, gplant_extension_adapter, GPLANT, EXTENSION_ADAPTER, GObject)

GPlantExtensionAdapter *gplant_extension_adapter_new                (GObject                *parent,
                                                                     PeasEngine             *engine,
                                                                     GType                   interface_type,
                                                                     const gchar            *key,
                                                                     const gchar            *value);
PeasEngine             *gplant_extension_adapter_get_engine         (GPlantExtensionAdapter *self);
gpointer                gplant_extension_adapter_get_extension      (GPlantExtensionAdapter *self);
GType                   gplant_extension_adapter_get_interface_type (GPlantExtensionAdapter *self);
const gchar            *gplant_extension_adapter_get_key            (GPlantExtensionAdapter *self);
void                    gplant_extension_adapter_set_key            (GPlantExtensionAdapter *self,
                                                                     const gchar            *key);
const gchar            *gplant_extension_adapter_get_value          (GPlantExtensionAdapter *self);
void                    gplant_extension_adapter_set_value          (GPlantExtensionAdapter *self,
                                                                     const gchar            *value);

G_END_DECLS
