#pragma once

#if !defined (GPLANT_PLUGINS_INSIDE) && !defined (GPLANT_PLUGINS_COMPILATION)
# error "Only <libgplant-plugins.h> can be included directly."
#endif

#include <libpeas/peas.h>
#include <libgplant-core.h>

G_BEGIN_DECLS

#define GPLANT_TYPE_EXTENSION_SET_ADAPTER (gplant_extension_set_adapter_get_type())

G_DECLARE_FINAL_TYPE (GPlantExtensionSetAdapter, gplant_extension_set_adapter, GPLANT, EXTENSION_SET_ADAPTER, GObject)

typedef void (*GPlantExtensionSetAdapterForeachFunc) (GPlantExtensionSetAdapter *set,
                                                      PeasPluginInfo            *plugin_info,
                                                      PeasExtension             *extension,
                                                      gpointer                   user_data);

GPlantExtensionSetAdapter *gplant_extension_set_adapter_new                 (GObject                              *parent,
                                                                             PeasEngine                           *engine,
                                                                             GType                                 interface_type,
                                                                             const gchar                          *key,
                                                                             const gchar                          *value);
PeasEngine                *gplant_extension_set_adapter_get_engine          (GPlantExtensionSetAdapter            *self);
GType                      gplant_extension_set_adapter_get_interface_type  (GPlantExtensionSetAdapter            *self);
const gchar               *gplant_extension_set_adapter_get_key             (GPlantExtensionSetAdapter            *self);
void                       gplant_extension_set_adapter_set_key             (GPlantExtensionSetAdapter            *self,
                                                                             const gchar                          *key);
const gchar               *gplant_extension_set_adapter_get_value           (GPlantExtensionSetAdapter            *self);
void                       gplant_extension_set_adapter_set_value           (GPlantExtensionSetAdapter            *self,
                                                                             const gchar                          *value);
guint                      gplant_extension_set_adapter_get_n_extensions    (GPlantExtensionSetAdapter            *self);
void                       gplant_extension_set_adapter_foreach             (GPlantExtensionSetAdapter            *self,
                                                                             GPlantExtensionSetAdapterForeachFunc  foreach_func,
                                                                             gpointer                              user_data);
void                       gplant_extension_set_adapter_foreach_by_priority (GPlantExtensionSetAdapter            *self,
                                                                             GPlantExtensionSetAdapterForeachFunc  foreach_func,
                                                                             gpointer                              user_data);
PeasExtension             *gplant_extension_set_adapter_get_extension       (GPlantExtensionSetAdapter            *self,
                                                                             PeasPluginInfo                       *plugin_info);

G_END_DECLS
