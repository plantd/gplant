#pragma once

#include <libgplant-core.h>

G_BEGIN_DECLS

#define GPLANT_PLUGINS_INSIDE

#include "gp-extension-adapter.h"
#include "gp-extension-set-adapter.h"

#undef GPLANT_PLUGINS_INSIDE

G_END_DECLS
