project('gplant', 'c',
          license: 'MIT',
          version: '0.1.0',
    meson_version: '>= 0.50.0',
  default_options: [ 'warning_level=1', 'buildtype=debugoptimized', 'c_std=gnu11' ],
)

version_split = meson.project_version().split('.')
MAJOR_VERSION = version_split[0]
MINOR_VERSION = version_split[1]
MICRO_VERSION = version_split[2]

libgplant_api_version = '@0@.@1@'.format(MAJOR_VERSION, MINOR_VERSION)

pkgdocdir_abs = join_paths(get_option('prefix'), get_option('datadir'), 'doc', 'gplant')
pkglibdir_abs = join_paths(get_option('prefix'), get_option('libdir'), 'gplant')
pkglibdir = join_paths(get_option('libdir'), 'gplant')
pkgincludedir = join_paths(get_option('includedir'), 'gplant')
pkggirdir = join_paths(get_option('datadir'),'gplant', 'gir-1.0')
pkgtypelibdir = join_paths(get_option('libdir'), 'gplant', 'girepository-1.0')
pkgvapidir = join_paths(get_option('datadir'), 'gplant/vapi')

# XXX: are these needed anymore?
localedir = join_paths (get_option ('prefix'), get_option ('localedir'))
datadir = join_paths (get_option ('prefix'), get_option ('datadir'))

config_h = configuration_data()
config_h.set_quoted('PACKAGE_NAME', 'gplant')
config_h.set_quoted('PACKAGE_ABI_S', libgplant_api_version)
config_h.set('PACKAGE_ABI', libgplant_api_version)
config_h.set_quoted('PACKAGE_VERSION', meson.project_version())
config_h.set_quoted('PACKAGE_STRING', 'gplant-' + meson.project_version())
config_h.set_quoted('PACKAGE_DATADIR', join_paths(get_option('prefix'), get_option('datadir')))
config_h.set_quoted('PACKAGE_ICONDIR', join_paths(get_option('prefix'), get_option('datadir'), 'gplant/icons'))
config_h.set_quoted('PACKAGE_DOCDIR', join_paths(get_option('prefix'), get_option('datadir'), 'doc/gplant'))
config_h.set_quoted('PACKAGE_LIBDIR', join_paths(get_option('prefix'), get_option('libdir')))
config_h.set_quoted('PACKAGE_LOCALE_DIR', join_paths(get_option('prefix'), get_option('datadir'), 'locale'))
config_h.set_quoted('PACKAGE_LIBEXECDIR', join_paths(get_option('prefix'), get_option('libexecdir')))

config_h.set('GETTEXT_PACKAGE', 'PACKAGE_NAME')
config_h.set('LOCALEDIR', 'PACKAGE_LOCALE_DIR')

add_global_arguments([
  '-DHAVE_CONFIG_H',
  '-I' + meson.build_root(), # config.h
  '-D_GNU_SOURCE',
  '-DGPLANT_COMPILATION',
], language: 'c')

libgplant_args = []

# common dependencies
dep_gtk = dependency('gtk+-3.0', version: '>= 3.20')
dep_gio = dependency('gio-2.0', version: '>= 2.40')
dep_dazzle = dependency('libdazzle-1.0', version: '>= 3.20')
#dep_apex = dependency('libapex-1.0', '>= 0.2')
dep_peas = dependency('libpeas-1.0', version: '>= 1.22.0')

configure_file(output: 'config.h', configuration: config_h)

gnome = import('gnome')
i18n = import('i18n')
pkgconfig = import('pkgconfig')

subdir('data')
subdir('src')
subdir('po')
